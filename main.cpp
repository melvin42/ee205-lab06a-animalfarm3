///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Melvin Alhambra <melvin42@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18/02/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animalfactory.hpp"
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 3" << endl;
   
   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   for (int i = 0 ; i < 25 ; i++ ){
      animalArray[i] = AnimalFactory::getRandomAnimal();	
   }
   cout << boolalpha << endl;
   cout << "Array of Animals:"       << endl;
   cout << "   Is it empty: "        << animalArray.empty()    << endl;
   cout << "   Number of elements: " << animalArray.size()     << endl;
   cout << "   Max size: "           << animalArray.max_size() << endl;
   
   for( Animal* animal : animalArray ) {
      if(animal != NULL){
      cout << animal->speak() << endl;
      }
   }
   list<Animal*> animalList;
   for (int i = 0; i < 25; i++) {
        animalList.push_front(AnimalFactory::getRandomAnimal());
   }
   cout << boolalpha << endl;

   cout << "List of Animals:"        << endl;
   cout << "   Is it empty: "        << animalList.empty()    << endl;
   cout << "   Number of elements: " << animalList.size()     << endl;
   cout << "   Max size: "           << animalList.max_size() << endl;

   
   for ( Animal* animal : animalList) {      
      if (animal != NULL) {
         cout << animal->speak() << endl;
      }
   }
	return 0;
}
